#include "widget.h"
#include "laptoplistmodel.h"
#include <QVBoxLayout>
#include <QTableView>
#include <QSortFilterProxyModel>
#include <QSplitter>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QProgressBar>

Widget::Widget(QWidget *parent)
	: QWidget(parent)
{
	CpuListModel* cpuModel = new CpuListModel(this);
	m_laptopModel = new LaptopListModel(*cpuModel, this);

	QSortFilterProxyModel* cpuProxy = new QSortFilterProxyModel(this);
	cpuProxy->setSourceModel(cpuModel);
	QSortFilterProxyModel* laptopProxy = new QSortFilterProxyModel(this);
	laptopProxy->setSourceModel(m_laptopModel);

	QTableView* cpuTable = new QTableView(this);
	cpuTable->setModel(cpuProxy);
	cpuTable->setSortingEnabled(true);
	cpuTable->sortByColumn(2, Qt::DescendingOrder);
	connect(cpuModel, &QAbstractItemModel::modelReset, cpuTable, &QTableView::resizeColumnsToContents);

	QTableView* laptopTable = new QTableView(this);
	laptopTable->setModel(laptopProxy);
	laptopTable->setSortingEnabled(true);
	laptopTable->sortByColumn(8, Qt::AscendingOrder);
	connect(m_laptopModel, &QAbstractItemModel::modelReset, laptopTable, &QTableView::resizeColumnsToContents);

	QProgressBar* progressBar = new QProgressBar(this);
	progressBar->setRange(0, 1);
	progressBar->setValue(0);
	connect(m_laptopModel, &LaptopListModel::changeProgressValue, progressBar, &QProgressBar::setValue);
	connect(m_laptopModel, &LaptopListModel::changeProgressMax, progressBar, &QProgressBar::setMaximum);

	QSplitter* splitter = new QSplitter(Qt::Vertical, this);
	splitter->addWidget(cpuTable);
	splitter->addWidget(laptopTable);

	QVBoxLayout* vl = new QVBoxLayout();
	QHBoxLayout* hl = new QHBoxLayout();
	hl->addWidget(m_url = new QLineEdit(this));
	QPushButton* button = new QPushButton("Load", this);
	connect(button, &QPushButton::clicked, this, &Widget::reparseUrl);
	hl->addWidget(button);
	button = new QPushButton("Home", this);
	connect(button, &QPushButton::clicked, m_laptopModel, &LaptopListModel::getLaptopList);
	hl->addWidget(button);

	vl->addLayout(hl);
	vl->addWidget(splitter);
	vl->addWidget(progressBar);
	setLayout(vl);

	splitter->setSizes(QList<int>() << 0 << 1);
}

Widget::~Widget()
{

}

void Widget::reparseUrl()
{
	m_laptopModel->getLaptopListFromUrl(m_url->text());
}
