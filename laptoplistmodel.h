#ifndef LAPTOPLISTMODEL_H
#define LAPTOPLISTMODEL_H

#include <QString>
#include <QModelIndex>
#include <QVariant>

class QWebEnginePage;

class CpuRate
{
public:
	QString name;
	QString cpuId;
	int rate = 1;
	QString link;
};


class LaptopInfo
{
public:
	QString name;
	QString characteristics;
	int ram = 0;
	int hdd = 0;
	int cpuIndex = -1;
	QString cpuId;
	int price = 0;
	int laptopRate = 0;
	QString link;
};


class CpuListModel : public QAbstractTableModel
{
	Q_OBJECT
public:
	CpuListModel(QObject* parent = nullptr);

	virtual int rowCount(const QModelIndex& parentIndex = QModelIndex()) const override;
	virtual int columnCount(const QModelIndex& parent = QModelIndex()) const override;

	virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

	int getCpuIndex(QString cpuName);
	CpuRate getCpuInfo(int index) const;

signals:
	void cpuListIsReady();

public slots:
	void replyFinished();
	void parseCompleted(const QString &result);

private:
	void getCpuRates();

	QVector<CpuRate> m_cpuList;
	QMap<QString, int> m_cpuMap;
	QWebEnginePage* m_webPage;
};


class LaptopListModel : public QAbstractTableModel
{
	Q_OBJECT
public:
	LaptopListModel(CpuListModel& cpuListModel, QObject* parent = nullptr);

	virtual int rowCount(const QModelIndex& parentIndex = QModelIndex()) const override;
	virtual int columnCount(const QModelIndex& parent = QModelIndex()) const override;

	virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

signals:
	void changeProgressValue(int value);
	void changeProgressMax(int max);

public slots:
	void replyFinished();
	void getLaptopList();
	void getLaptopListFromUrl(QString url);

private:
	void parseRozetka();
	void parseCompleted(const QString& result);
	void sendRequest(const QString& url);

	CpuListModel& m_cpuListModel;
	QVector<LaptopInfo> m_laptopList;
	QWebEnginePage* m_webPage;
	QStringList m_visitedPages;
	QStringList m_pagesToVisit;
};

#endif // LAPTOPLISTMODEL_H
