#include "laptoplistmodel.h"
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <cctype>
#include <QDomDocument>
#include <QRegularExpression>
#include <assert.h>
#include <QInputDialog>
#include <QWebEnginePage>
#include <QMessageBox>


QString getSubString(const QString& source, int& position, const QString& startStr, const QString& finishStr)
{
	int start = source.indexOf(startStr, position) + startStr.length();
	int finish = source.indexOf(finishStr, start);
	position = finish + finishStr.length();
	return source.mid(start, finish - start);
}


QString getCode(QString cpuName)
{
	QStringList cpuWords = cpuName.split(" ");
	int maxDigits = 0;
	QString code;

	for (int i = cpuWords.count() - 1; i >= 0 ; i--)
	{
		QString& key = cpuWords[i];
		if (key.contains("(") || key.contains(")"))
		{
			continue;
		}

		if (key.length() > 1 && key.contains("@"))
		{
			key = key.split("@")[0];
		}

		if (key == "@" || key.contains("Hz"))
		{
			continue;
		}

		if (code.isEmpty())
		{
			code = key;
		}

		int digitCount = 0;
		for (int j = 0; j < key.count(); j++)
		{
			if (std::isdigit(key[j].toLatin1()))
			{
				digitCount++;
			}
		}

		if (digitCount > maxDigits)
		{
			maxDigits = digitCount;
			code = key;
		}
	}

	return code.toUpper();
}


QList<QDomNode> getElementsByAttribute(QDomDocument domDocument, QString tagName, QString attributeName, QString attributeValue)
{
	QList<QDomNode> result;

	QDomNodeList tagNodeList = domDocument.elementsByTagName(tagName);

	for (int i = 0; i < tagNodeList.size(); ++i) {
		QDomElement domElement = tagNodeList.at(i).toElement();
		QDomAttr attribute = domElement.attributeNode(attributeName);
		if (!attribute.isNull() && attribute.value() == attributeValue)
		{
			result.push_back(tagNodeList.at(i));
		}
	}

	return result;
}


QList<QDomNode> getElementsByAttribute(QDomElement domElement, QString tagName, QString attributeName, QString attributeValue)
{
	QList<QDomNode> result;

	QDomNodeList tagNodeList = domElement.elementsByTagName(tagName);

	for (int i = 0; i < tagNodeList.size(); ++i) {
		QDomElement domElement = tagNodeList.at(i).toElement();
		QDomAttr attribute = domElement.attributeNode(attributeName);
		if (!attribute.isNull() && attribute.value() == attributeValue)
		{
			result.push_back(tagNodeList.at(i));
		}
	}

	return result;
}


QList<QDomNode> getElementsByClass(QDomNode domNode, QString tagName, QString className)
{
	QList<QDomNode> result;

	if (!domNode.isElement())
	{
		return result;
	}

	QDomNodeList&& tagNodeList = domNode.toElement().elementsByTagName(tagName);

	for (int i = 0; i < tagNodeList.size(); ++i) {
		QDomElement&& domElement = tagNodeList.at(i).toElement();
		QDomAttr&& attribute = domElement.attributeNode("class");
		if (attribute.isNull())
		{
			continue;
		}
		QString attributeValue = attribute.value();
		if (attribute.value().split(" ", QString::SkipEmptyParts).contains(className))
		{
			result.push_back(tagNodeList.at(i));
		}
	}

	return result;
}


QList<QDomNode> getElementsByClass(QDomElement domElement, QString tagName, QString className)
{
	QList<QDomNode> result;

	QDomNodeList&& tagNodeList = domElement.elementsByTagName(tagName);

	for (int i = 0; i < tagNodeList.size(); ++i) {
		QDomElement&& domElement = tagNodeList.at(i).toElement();
		QDomAttr&& attribute = domElement.attributeNode("class");
		if (attribute.isNull())
		{
			continue;
		}
		if (attribute.value().split(" ", QString::SkipEmptyParts).contains(className))
		{
			result.push_back(tagNodeList.at(i));
		}
	}

	return result;
}


CpuListModel::CpuListModel(QObject* parent) :
	QAbstractTableModel(parent)
{
	getCpuRates();
}

int CpuListModel::rowCount(const QModelIndex&) const
{
	return m_cpuList.count();
}

int CpuListModel::columnCount(const QModelIndex&) const
{
	return 4;
}

QVariant CpuListModel::data(const QModelIndex& index, int role) const
{
	if (role == Qt::DisplayRole && index.row() < m_cpuList.count())
	{
		const CpuRate& cpu = m_cpuList[index.row()];
		switch (index.column())
		{
		case 0: return cpu.name;
		case 1: return cpu.cpuId;
		case 2: return cpu.rate;
		case 3: return cpu.link;
		}
	}
	return QVariant();
}

QVariant CpuListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			switch (section)
			{
			case 0: return tr("Name");
			case 1: return tr("ID");
			case 2: return tr("Rate");
			case 3: return tr("Link");
			}
		}
		else
		{
			return QString::number(section + 1);
		}
	}
	return QVariant();
}

int CpuListModel::getCpuIndex(QString cpuName)
{
	return m_cpuMap[getCode(cpuName)];
}

CpuRate CpuListModel::getCpuInfo(int index) const
{
	if (index >= 0 && index < m_cpuList.count())
	{
		return m_cpuList[index];
	}
	return CpuRate();
}

void CpuListModel::replyFinished()
{
	m_webPage->runJavaScript("function dataGetter() {"
							 "  var rows = document.querySelectorAll('#cputable tbody tr');"
							 "  var result = '';"
							 "  for (var i = 0; i < rows.length; i++) {"
							 "    var row = rows[i];"
							 "    var link = row.getElementsByTagName('a')[0];"
							 "    var points = row.getElementsByTagName('td')[1].textContent;"
							 "    result += link.getAttribute('href') + '|';"
							 "    result += link.textContent + '|';"
							 "    result += points + '\\n';"
							 "  };"
							 "  return result;"
							 "}"
							 "dataGetter();",
							 [this](const QVariant &result){ this->parseCompleted(result.toString());
	});
}

void CpuListModel::parseCompleted(const QString& result)
{
	beginResetModel();
	QStringList cpuList = result.split('\n');
	foreach (const QString& cpuInfo, cpuList) {
		QStringList cpuInfoData = cpuInfo.split('|');
		if (cpuInfoData.size() < 3) {
			continue;
		}
		CpuRate cpu;
		cpu.link = "http://www.cpubenchmark.net/" + cpuInfoData[0];
		cpu.name = cpuInfoData[1];
		cpu.cpuId = getCode(cpu.name);

		cpu.rate = cpuInfoData[2].replace(QLatin1Literal(","), QLatin1Literal("")).toInt();
		if (cpu.rate == 0)
		{
			continue;
		}
		m_cpuMap.insert(cpu.cpuId, m_cpuList.count());
		if (cpu.cpuId.contains("-"))
		{
			m_cpuMap.insert(cpu.cpuId.split("-")[1], m_cpuList.count());
		}

		m_cpuList.append(cpu);
	}
	endResetModel();
}

void CpuListModel::getCpuRates()
{
	m_webPage = new QWebEnginePage(this);
	connect(m_webPage, &QWebEnginePage::loadFinished, this, &CpuListModel::replyFinished);

	m_webPage->load(QUrl("https://www.cpubenchmark.net/cpu_list.php"));
}


LaptopListModel::LaptopListModel(CpuListModel& cpuListModel, QObject* parent) :
	QAbstractTableModel(parent),
	m_cpuListModel(cpuListModel)
{
	connect(&cpuListModel, &CpuListModel::modelReset, this, &LaptopListModel::getLaptopList);

	m_webPage = new QWebEnginePage(this);
	connect(m_webPage, &QWebEnginePage::loadFinished, this, &LaptopListModel::replyFinished);
}

int LaptopListModel::rowCount(const QModelIndex&) const
{
	return m_laptopList.count();
}

int LaptopListModel::columnCount(const QModelIndex&) const
{
	return 10;
}

QVariant LaptopListModel::data(const QModelIndex& index, int role) const
{
	int row = index.row();
	if (role == Qt::DisplayRole && row < m_laptopList.count() && row > 0)
	{
		const LaptopInfo& laptop = m_laptopList[row];
		const CpuRate& cpu = m_cpuListModel.getCpuInfo(laptop.cpuIndex);
		switch (index.column())
		{
		case 0: return laptop.name;
		case 1: return laptop.characteristics;
		case 2: return cpu.name;
		case 3: return laptop.cpuId;
		case 4: return cpu.rate;
		case 5: return laptop.ram;
		case 6: return laptop.hdd;
		case 7: return laptop.price;
		case 8: return double(laptop.price) / cpu.rate;
		case 9: return laptop.link;
		}
	}
	return QVariant();
}

QVariant LaptopListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			switch (section)
			{
			case 0: return tr("Name");
			case 1: return tr("Characteristics");
			case 2: return tr("CPU name");
			case 3: return tr("CPU ID");
			case 4: return tr("CPU rate");
			case 5: return tr("RAM");
			case 6: return tr("HDD");
			case 7: return tr("Price");
			case 8: return tr("Laptop rate");
			case 9: return tr("Link");
			}
		}
		else
		{
			return QString::number(section + 1);
		}
	}
	return QVariant();
}

void LaptopListModel::replyFinished()
{
	if (m_webPage->url().host().contains("rozetka.com.ua"))
	{
		parseRozetka();
	}
}

void LaptopListModel::getLaptopList()
{
	m_visitedPages.clear();
	getLaptopListFromUrl("https://rozetka.com.ua/notebooks/c80004/");
}

void LaptopListModel::getLaptopListFromUrl(QString url)
{
	beginResetModel();
	m_laptopList.clear();
	endResetModel();
	sendRequest(url);
}

void LaptopListModel::parseRozetka()
{
	m_webPage->runJavaScript("function dataGetter() {"
							 "  var result = '';"
							 "  var links = document.querySelectorAll('a.pagination__link');"
							 "  for (var i = 0; i < links.length; i++) {"
							 "    result += links[i].getAttribute('href') + '|';"
							 "  }"
							 "  result += '%resultSeparator%';"
							 "  var rows = document.querySelectorAll('div.goods-tile');"
							 "  for (var i = 0; i < rows.length; i++) {"
							 "    var row = rows[i];"
							 "    var links = row.querySelectorAll('a.goods-tile__heading');"
							 "    var prices = row.querySelectorAll('span.goods-tile__price-value');"
							 "    var infos = row.querySelectorAll('div.goods-tile__hidden-content p');"
							 "    if (links.length == 0 || prices.length == 0 || infos.length == 0)"
							 "      continue;"
							 "    var link = links[0];"
							 "    var price = prices[0].textContent;"
							 "    var info = infos[0].textContent;"
							 "    result += link.getAttribute('href') + '|';"
							 "    result += link.getAttribute('title') + '|';"
							 "    result += info + '|';"
							 "    result += price + '\\n';"
							 "  };"
							 "  return result;"
							 "}"
							 "dataGetter();",
							 [this](const QVariant &result){
		QStringList resultList = result.toString().split("%resultSeparator%");
		QString currentLink = this->m_webPage->url().toString();
		if (resultList.size() == 2) {
			QStringList links = resultList[0].split('|', QString::SkipEmptyParts);
			for (QString link : links) {
				if (m_visitedPages.contains(link) || m_pagesToVisit.contains(link)) {
					continue;
				}
				if (!link.startsWith("http")) {
					continue;
				}
				m_pagesToVisit.push_back(link);
			}
			this->parseCompleted(resultList[1]);
		} else {
			QMessageBox::information(nullptr, "Something is wrong", "Got such answer:\n"+resultList.join('\n')+"\nTrying get url: " + currentLink + "\nLets try again...");
			sendRequest(currentLink);
			return;
		}
		if (m_pagesToVisit.contains(currentLink)) {
			m_pagesToVisit.removeAll(currentLink);
		}
		m_visitedPages.push_back(currentLink);
		if (!m_pagesToVisit.isEmpty()) {
			sendRequest(m_pagesToVisit.first());
		}
		emit changeProgressMax(m_visitedPages.count() + m_pagesToVisit.count());
		emit changeProgressValue(m_visitedPages.count());
	});
}

void LaptopListModel::parseCompleted(const QString& result)
{
	beginResetModel();
	QStringList laptops = result.split('\n', QString::SkipEmptyParts);
	for (const QString& laptop : laptops)
	{
		QStringList laptopData = laptop.split('|');
		if (laptopData.size() < 4)
		{
			continue;
		}
		LaptopInfo laptopInfo;
		laptopInfo.link = laptopData[0];
		laptopInfo.name = laptopData[1];
		QString priceText = laptopData[3].trimmed().remove(QRegExp("\\D"));
		laptopInfo.price = priceText.toInt();
		laptopInfo.characteristics = laptopData[2].trimmed();

		bool alreadyExists = false;
		for (const auto& laptop : m_laptopList) {
			if (laptop.characteristics == laptopInfo.characteristics &&
					laptop.price == laptopInfo.price &&
					laptop.name == laptopInfo.name) {	// May be different color with same price and characteristics
				alreadyExists = true;
				break;
			}
		}

		if (alreadyExists) {
			continue;
		}

		QStringList characteristics = laptopInfo.characteristics.split("/");

		if (characteristics.size() < 2) {
			continue;
		}

		for (const QString& characteristic : characteristics)
		{
			int position = 0;
			if (characteristic.contains("RAM", Qt::CaseInsensitive))
			{
				laptopInfo.ram = getSubString(characteristic, position, "RAM ", "ГБ").trimmed().toInt();
			}
			if (characteristic.contains("HDD", Qt::CaseInsensitive))
			{
				if (characteristic.contains("ТБ", Qt::CaseInsensitive))
				{
					laptopInfo.hdd = int(getSubString(characteristic, position, "HDD ", "ТБ").trimmed().toDouble() * 1000);
				}
				else
				{
					laptopInfo.hdd = getSubString(characteristic, position, "HDD ", "ГБ").trimmed().toInt();
				}
			}
			if (characteristic.contains("SSD", Qt::CaseInsensitive) && laptopInfo.hdd == 0)
			{
				laptopInfo.hdd = getSubString(characteristic, position, "SSD ", "ГБ").trimmed().toInt();
			}
			if (laptopInfo.cpuIndex == -1 && (characteristic.contains("Intel") || characteristic.contains("AMD")))
			{
				laptopInfo.cpuId = getCode(characteristic);
				laptopInfo.cpuIndex = m_cpuListModel.getCpuIndex(characteristic);
			}
		}

		m_laptopList.append(laptopInfo);
	}
	endResetModel();
}

void LaptopListModel::sendRequest(const QString& url)
{
	m_webPage->load(url);
}



