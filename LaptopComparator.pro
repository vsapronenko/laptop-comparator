#-------------------------------------------------
#
# Project created by QtCreator 2015-05-24T01:23:13
#
#-------------------------------------------------

QT       += core gui network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets webenginewidgets

TARGET = LaptopComparator
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    laptoplistmodel.cpp

HEADERS  += widget.h \
    laptoplistmodel.h

#c++11 support for GCC
#
gcc:QMAKE_CXXFLAGS += -std=c++11
