#ifndef WIDGET_H
#define WIDGET_H

class QLineEdit;
class LaptopListModel;

#include <QWidget>

class Widget : public QWidget
{
	Q_OBJECT

public:
	Widget(QWidget *parent = 0);
	~Widget();

private:
	QLineEdit* m_url;
	LaptopListModel* m_laptopModel;

public slots:
	void reparseUrl();
};

#endif // WIDGET_H
